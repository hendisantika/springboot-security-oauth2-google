package com.hendisantika.springbootsecurityoauth2google.service;

import com.hendisantika.springbootsecurityoauth2google.model.User;
import com.hendisantika.springbootsecurityoauth2google.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-oauth2-google
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/02/20
 * Time: 07.05
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User, Long> implements UserService {

    @Autowired(required = true)
    UserRepository userRepository;

    @Override
    public User findByEmailAddress(String emailAddress) {
        return userRepository.findByEmailAddress(emailAddress);
    }

    @Override
    public List<User> findByName() {
        // TODO Auto-generated method stub
        return userRepository.findByName();
    }

}