package com.hendisantika.springbootsecurityoauth2google.service;

import com.hendisantika.springbootsecurityoauth2google.repository.BaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-oauth2-google
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/02/20
 * Time: 07.04
 */
@Transactional
public class BaseServiceImpl<T, ID extends Serializable> implements BaseService<T, ID> {

    @Autowired(required = true)
    BaseRepository<T, ID> baseRepository;

    @Override
    public <S extends T> S save(S entity) {
        return baseRepository.save(entity);
    }

    @Override
    public Optional<T> findById(ID primaryKey) {
        // TODO Auto-generated method stub
        return baseRepository.findById(primaryKey);
    }

    @Override
    public List<T> findAll() {
        // TODO Auto-generated method stub
        return baseRepository.findAll();
    }

    @Override
    public long count() {
        // TODO Auto-generated method stub
        return baseRepository.count();
    }

    @Override
    public void delete(T entity) {
        // TODO Auto-generated method stub
        baseRepository.delete(entity);
    }

    @Override
    public boolean existsById(ID primaryKey) {
        // TODO Auto-generated method stub
        return baseRepository.existsById(primaryKey);
    }

}