package com.hendisantika.springbootsecurityoauth2google.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-oauth2-google
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/02/20
 * Time: 07.03
 */
public interface BaseService<T, ID extends Serializable> {

    <S extends T> S save(S entity);

    Optional<T> findById(ID primaryKey);

    List<T> findAll();

    long count();

    void delete(T entity);

    boolean existsById(ID primaryKey);
}