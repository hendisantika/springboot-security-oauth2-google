package com.hendisantika.springbootsecurityoauth2google.service;

import com.hendisantika.springbootsecurityoauth2google.model.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-oauth2-google
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/02/20
 * Time: 07.05
 */
public interface UserService extends BaseService<User, Long> {

    User findByEmailAddress(String emailAddress);

    List<User> findByName();
}