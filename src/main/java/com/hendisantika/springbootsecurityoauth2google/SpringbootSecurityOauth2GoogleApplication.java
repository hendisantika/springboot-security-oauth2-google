package com.hendisantika.springbootsecurityoauth2google;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class SpringbootSecurityOauth2GoogleApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SpringbootSecurityOauth2GoogleApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringbootSecurityOauth2GoogleApplication.class, args);
    }

}
