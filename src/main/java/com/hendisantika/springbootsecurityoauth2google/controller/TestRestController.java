package com.hendisantika.springbootsecurityoauth2google.controller;

import com.hendisantika.springbootsecurityoauth2google.model.User;
import com.hendisantika.springbootsecurityoauth2google.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-oauth2-google
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/02/20
 * Time: 07.13
 */
@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
@Slf4j
public class TestRestController {

    private final UserService userService;

    @RequestMapping("/create")
    @ResponseBody
    public User create(User user) {
        user.setName("Uzumaki Naruto");
        user.setEmail("uzumaki_naruto@konohagakure.co.jp");
        user.setAge(31);

        // user = userRepository.save(user);
        user = userService.save(user);
        return user;
    }

    @GetMapping("/read")
    public List<User> read() {
        User user1 = userService.findByEmailAddress("uzumaki_naruto@konohagakure.co.jp");
        log.info("get obj " + user1.getName());

        List<User> listUser = userService.findByName();

        listUser.forEach(item -> log.info("user " + item.getName()));
        return userService.findAll();
    }

    @GetMapping("/update")
    public User update(@RequestParam Long userID) {
        Optional<User> user = userService.findById(userID);

        user.get().setName("henry1");
        User obj = userService.save(user.get());
        return obj;
    }

    @RequestMapping("/delete")
    public String delete(@RequestParam Long userID) {
        User user = new User();
        user.setId(userID);

        userService.delete(user);
        return "user " + userID + " deleted successfully";
    }

    @GetMapping("/user")
    public Principal user(Principal user) {
        return user;
    }
}
