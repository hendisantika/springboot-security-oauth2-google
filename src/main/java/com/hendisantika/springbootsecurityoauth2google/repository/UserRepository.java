package com.hendisantika.springbootsecurityoauth2google.repository;

import com.hendisantika.springbootsecurityoauth2google.model.User;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-security-oauth2-google
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 01/02/20
 * Time: 07.01
 */
public interface UserRepository extends BaseRepository<User, Long> {

    @Query("select u from User u where u.email = ?1")
    User findByEmailAddress(String emailAddress);

    @Query(value = "SELECT * FROM user_acl u WHERE u.name = 'Henry'", nativeQuery = true)
    List<User> findByName();
}